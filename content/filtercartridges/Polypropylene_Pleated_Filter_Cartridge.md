+++
showonlyimage = false
draft = false
image = "img/filtercartridges/polypropylene-pleated-cartridge.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Polypropylene Pleated Filter Cartridge"
weight = 1
+++

Polypropylene filter cartridges are precisely manufactured for use in critical filtration applications within food, pharmaceuticals, bio-tech, dairy, beverages, brewing, semiconductor, water treatment & other demanding process industries.
<!--more-->

{{< figure src="/img/filtercartridges/polypropylene-pleated-cartridge.jpg" class="img-shadow" >}}
---


Polypropylene pleated cartridges use the very latest gradient density micro fiber media technology to provide a combination of excellent micro ratings, high flow rates and high contaminant holding capacities. A special combination of polypropylene media with variation in the fiber diameter has created a gradient density matrix, ranging from open on the outside to finer on the inside, thereby providing a filter with in filter, which considerably increases contamination holding capacity & throughput.

All components used in the manufacture are biologically safe, chemically inert and meet FDA and other international quality requirements. Polypropylene offers an extremely broad chemical compatibility making it suitable for many applications.

##### SALIENT FEATURES
* All polypropylene construction
* Absolute & nominal efficiency
* 0.1 to 40 micron ratings
* Gradient density micron fiber media
* High surface area more than 0.5 m2
* Robust outer cage
* Biologically safe
* Non fibber migration
* Thermally welded construction
* FDA approved filters
* End connections to fit all standard housings technical specifications

##### TECHNICAL
* Sizes ⇒ 10” / 20” / 30” / 40” Long
* Micron Rating ⇒ 0.1 / 0.2 / 0.45 / 1 to 50
* Outside Diameter ⇒ 69 mm
* Inner Diameter ⇒ 28 mm

##### CONFIGURATION
* Double Open Type (DOE type)
* Code 7 F (226 O Ring design)
* Code 7 S (226 O Ring / Flat design)
* Code 3 F (222 O Ring design)
* Code 3 S (222 O Ring / Flat design)

##### APPLICATIONS
* Fine Chemicals and Pharmaceuticals
* D.I. Water
* R.O. Pre-filtration
* Ophthalmic Liquids
* Biological Fluids
* Oral Drugs
* Photographic Film & Paper
* Anti-Halation Coatings
* Developer Chemicals
* Emulsions
* Gelatin
* Beverages (Wine / Beer / Fruit Juices / Alcohols)
