+++
showonlyimage = false
draft = false
image = "img/filtercartridges/stainless-steel-cartridge.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Stainless Steel Filter Cartridge"
weight = 4
+++

Stainless steel cartridges are designed to overcome the temperature and chemical compatibility limitations of fabric or synthetic fiber media. 
<!--more-->
This will offers very high temperature resistance & can withstand high differential pressure.

{{< figure src="/img/filtercartridges/stainless-steel-cartridge.jpg" class="img-shadow" >}}
---

Stainless steel cartridges are offered in SS 304, SS 316 & SS 316 material. These elements can be plain cylindrical or pleated configuration to increase filtration area.

Normally all stainless steel pleated & cylindrical filters are supported with coarser filter media to ensure any direct damages to main filtering media under process upsets. A bubble point tests can be done to certify that no opening larger than the specified pore size exist in product joints or seams.  No media migration due to stainless steel material. These elements can be washed & reused.


##### FILTER TYPES
* Stainless steel wire mesh
* Stainless steel sintered metal
* Stainless steel random fiber
* Sintered metal fiber type
* Plain cylindrical design
* Pleated configuration design



##### SALIENT FEATURES
* Stable pore shapes
* High permeability
* High dirt-holding capacity (longer lifetime)
* High temperature resistance
* High differential pressure with stand capacity
* Strong corrosion resistant
* Back flushing
* Excellent mechanical strength
* No media migration
* Size customization



##### CONSTRUCTION
* The composite fiber material is then sintered together with a wire mesh under vacuum conditions and rolled to mats of a specific thickness.
* Stainless Cartridge can be wrought into tubes, cartridges or disks, plain, pleated or according to customer requests.



##### TECHNICAL
* Sizes ⇒ 10” / 20” / 30” / 40” Long (available in custom sizes as well)
* Micron Rating ⇒ 0.5 / 1 / 3 / 5 / 10 / 20 / 25 / 50 & More
* Outside Diameter ⇒ 64 mm
* Inner Diameter ⇒ 28 mm



##### CONFIGURATION
* Double Open Type (DOE type)
* Code 7 F (226 O Ring design)
* Code 7 S (226 O Ring / Flat design)
* Code 3 F (222 O Ring Design) • Code 3 S (222 O Ring / Flat design)
* 1” NPT / BSP Connection



##### APPLICATIONS
* Catalyst recovery in Petrochemical / Chemical industries
* Polymer Filtration
* Cross Flow Filtration
* Gas Filtration
* Analytic Devices
* Medical devices
* Oil Filtration
* Hot Gas Filtration
* Aerosol Application
* Fuel & Hydraulic Oil Filtration
* Gas – Liquid Separation
* R.O. Pre Filtration