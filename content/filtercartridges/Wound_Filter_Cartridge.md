+++
showonlyimage = false
draft = false
image = "img/filtercartridges/wound-filter-cartridge.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Wound Filter Cartridge"
weight = 5
+++

Wound cartridges are designed to meet the most demanding filtration duties.
<!--more-->

They offer an economic, compact, easily installed and maintained filtration system for removal of particulate removal from liquid.

{{< figure src="/img/filtercartridges/wound-filter-cartridge.jpg" class="img-shadow" >}}
---

Wound cartridges are manufactured from a variety of carefully selected raw materials. These are processed into fibers of specific grades using the latest technology.

After carding & spinning into roving they are wound into cartridges with carefully controlled micron rating. From raw material to finished products we are in control of the quality and filtration characteristics. They are appreciated due to high dirt holding capacity and to the rugged construction which allow facing different application in liquid and gas filtration. Operation Unfiltered fluid passes through depth filter matrix, enables the progressive retention of finer particles, providing high efficiency, high dirt retention & filter life. Fluid flows from outside to inside through filter media.


##### SALIENT FEATURES
* Standard and customizes sizes fit most housings
* High strength & pressure resistance
* Manufactured in continuous length
* Full range of sizes from 10” to 60” long
* Excellent flow with low pressure drop
* High dirt holding capacity
* Compatible with a wide range of fluids
* NSF & FDA approved



##### CONSTRUCTION
* Polypropylene with polypropylene core
* Cotton with stainless steel core



##### TECHNICAL
* Sizes ⇒ 10” / 20” / 30” / 40” / 60” Long
* Micron Rating ⇒ 0.5 to 100 m
* Outside Diameter ⇒ 58 mm to 200 mm
* Inner Diameter ⇒ 25 mm



##### CONFIGURATION
* Double Open Type (DOE Type)
* Code 7 (226 O Ring Design)



##### APPLICATIONS
* Polypropylene wound filter cartridge
* Organic Acids
* Oils
* Concentrated Alkalies
* Water
* Organic solvents
* Electroplating solution
* Cotton wound filter cartridge
* Vegetable Oils
* Dilute Acids
* Organic solvents
* Portable liquids
* Alkalies