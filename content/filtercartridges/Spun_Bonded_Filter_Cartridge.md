+++
showonlyimage = false
draft = false
image = "img/filtercartridges/spun-bonded-filter-cartridge.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Spun Bonded Filter Cartridge"
weight = 3
+++

Spun bonded filter cartridges are made of 100% polypropylene fibers. 
<!--more-->

The fibers have been carefully spun together to form a true gradient density from outer to inner surface. Filter cartridges available with core & without core version. The superior structure remains integral even under severe operating conditions and is no media migration.

{{< figure src="/img/filtercartridges/spun-bonded-filter-cartridge.jpg" class="img-shadow" >}}
---


Polypropylene fibers are blown continuous on central molded core, without the need for binders, resins or lubricants.  Operation Unfiltered fluid passes through depth filter matrix, enables the progressive retention of finer particles, providing high efficiency, high dirt retention & filter life. Fluid flows from outside to inside through filter media. Particulates are held securely in the filter matrix and clean fluid flows to the downstream side of cartridge.

##### SALIENT FEATURES
* Free of Surfactants, Binders and Adhesives
* Excellent flow with low pressure drop
* High dirt holding capacity
* High strength & pressure resistance
* 100 % Polypropylene for wide chemical compatibility
* Nominal & absolute filtration rating
* One piece construction up to 1016 mm & more



##### CONSTRUCTION
* Spun bonded filter cartridges are made of 100% polypropylene



##### TECHNICAL
* Sizes ⇒ 10” / 20” / 30” / 40” Long (available in custom sizes as well)
* Micron Rating ⇒ 0.5 / 1 / 5 / 10 / 20 / 25 / 50 / 75 /100
* Outside Diameter ⇒ 64 mm (2.5”) / 114 mm (4.5”) / 6” / 8”
* Inner Diameter ⇒ 25 mm / 28 mm / 48 mm



##### CONFIGURATION
* Double Open Type (DOE Type)
* Code 7 (226 O Ring Design)



##### APPLICATIONS
* Food & Beverages
* Pharmaceuticals
* Fine Chemicals
* Magnetic Coating
* Petrochemicals
* Cosmetics
* Water Treatment
* Metal Finishing
* Electronics
