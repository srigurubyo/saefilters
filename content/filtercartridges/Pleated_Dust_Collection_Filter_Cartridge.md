+++
showonlyimage = false
draft = false
image = "img/filtercartridges/pleated-dust-collection-cartridges.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Pleated Dust Collection Filter Cartridge"
weight = 2
+++

Pleated dust collection cartridges are made of 100 % polyester media with different coatings on it, as per requirement of applications. 
<!--more-->

Manufacturer of dust collectors originally designed to build the dust collection cartridges in equipment, where as pleated bags can be used for existing dust collector in place of conventional dust collection bags, without modifications.

{{< figure src="/img/filtercartridges/pleated-dust-collection-cartridges.jpg" class="img-shadow" >}}
---

This polyester media is made of high tenacity filament yarn without using any binding agent. Thus hard finished material is very stable against hot gas attach & structurally durable. This unique & hard finished material provides features of good pleat ability, high durability, and unique structure of very fine filament yarn which offer high performance in filtration. Thermally tight bonded media provide wash-ability, excellent pleat-ability, stable structure, fine dust release & good resistance of particle penetration in to the media.

This filter media are pleated in different pleated depths & height to accommodated desired filtration area.  Construction Pleated spun bonded filter media placed in a cylindrical configuration with support of metal core & encapsulated with metal end caps to form a pleated dust collection cartridges. The end caps are configured to mount as clean side or dirt side fitment. In case of metal caps these bags are supported with soft sponge rubber sealing to ensure positive sealing of filter while fitment.

##### SALIENT FEATURES
* High filtration area up to 20 m2
* Universal fixing arrangements
* Open pleat spacing
* Low consumption of compressed air for cleaning
* High efficiency due to surface filtration
* Very high air to cloth ratio
* Very high filtration efficiency


##### CONSTRUCTION
* PTFE coating for oil & moisture removal
* Aluminum coating for anti-static application
* Stainless steel coating for anti-static application
* Fire retardant coating
* PTFE lamination for very stick dust.



##### TECHNICAL
* Sizes ⇒ 300 mm to 1400 mm long as per customer’s requirement
* Diameter ⇒ 150 mm  / 225 mm / 325 mm
* Filtration area ⇒ 0.96 m2 to 20.0 m2



##### CONFIGURATION
* Flange type
* Three lug type
* Standard type



##### APPLICATIONS
* Pharmaceuticals
* Steel
* Power
* Powder coating
* Cement
* Chemicals
* Metals & Minerals
* Paints & Pigments