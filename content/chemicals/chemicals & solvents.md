+++
draft = false
image = "img/chemicals/chemical_testing.jpg"
showonlyimage = false
date = "2016-11-05T19:50:47+05:30"
title ="Chemicals & Solvents"
weight = 1
+++

Our approach to fine & speciality chemical distribution is to be your most reliable and resourceful partner. We pride ourselves on being ProActive Resourcing Specialists.
<!--more-->

#### Who We Are!

For decades as a chemical supplier, we've sourced raw materials from chemical suppliers around the world and marketed them to manufacturers serving key industrial and consumer marketplaces. These relationships are based on a long-standing chemicals expertise, exceptional follow-through and the fastest turnaround times in the business.

We are committed to exceeding environmental, health, safety and continuous improvement standards as set forth in the Responsible Distribution Process. We're a full-service chemical distributor partner helping our customers meet manufacturing and distribution requirements at all levels of the supply chain.

As a chemical supplier, we supply fine and speciality chemicals from our extensive network of global sourcing offices. We provide the broadest range of import/export, specialty chemical distribution to the chemical products industry. We are governed by the guiding principles of innovation, resourcefulness, proactiveness and accuracy.


#### Our Products
{{< figure src="/img/chemicals/chemicals.jpg" class="img-shadow" >}}
---
</br>

#### The company deals with a varied range of products, namely – such as:

* Anhydrous aluminum chloride
* Caustic soda lye
* Caustic soda flakes
* Hydrochloric Acid
* Sulfuric Acid 70% and 98%
* Amino acids & reagents
* Acetic Acid
* Nitric Acid
* Industrial salt
* Monochlorobenzene
* Bromophenol
* Dichlorobenzene reagent
* Oleum 23% and 65%,
* PAC Powder
* Hydrogen Peroxide
* Hydrochloric Acid
* Phosphoric Acid
* Isopropyl Alcohol
* Chloro Sulfuric Acid


##### All types of solvents such as:
* Toluene
* Acetone
* Chloroform
* MDC
* DMS
* DMF
* IPA
* Methanol
* Ethyl acetate

The name and business strength that has been earned by our organization today, is due to the professionally equipped and able staff, who have a varied experience and good knowledge of serving the customers to their satisfaction. This is further enhanced by the innovative ideas of the management, who strive from time to time in creating awareness and keeping the company at par to the latest development of today's world.
</br>

##### * We also supply specialised chemicals based on your custom requirement.*
