+++
draft = false
image = "img/modular/lenticular-filter-modules.jpg"
showonlyimage = false
date = "2016-11-05T19:50:47+05:30"
title = "Lenticular Filter System"
weight = 1
+++

Modular or Lenticular filter systems are designed using pile of depth filter pads and are separated using innovative in-build drainage structure.
<!--more-->

#### An Highly Efficient Filtration System

The divider between each sheet or separator increases the total stability of the filter cartridges by providing enough strength to the individual sheets.  This design also prevents the filter sheet deformities after heat treatments and adverse effects of contact with hot sanitation.  It is manufactured with rigid external clips to prevent damaging filter sheets during module loading and unloading while allowing for easy and reliable handling.

It is available in different specifications to suit the target application and the MOC compatibility.

#### Conventional Sparkler Filter System
{{< figure src="/img/modular/conventional_sparkler.jpg" class="img-shadow" >}}
---
</br>

#### Lenticular or Modular Filter System
{{< figure src="/img/modular/lenticular_filtration_1.jpg" class="img-shadow" >}}
---
</br>

#### Lenticular System - Cross Sectional View
{{< figure src="/img/modular/modular_cross_sectional_view.jpg" class="img-shadow" >}}
---
</br>

#### Lenticular System - Multi Directional Flow
{{< figure src="/img/modular/modular_flow.jpg" class="img-shadow" >}}
---
</br>

#### Salient Features
{{< figure src="/img/modular/modular_in_a_nutshell.jpg" class="img-shadow" >}}
---
