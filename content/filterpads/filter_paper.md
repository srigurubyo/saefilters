+++
date = "2016-11-05T19:41:01+05:30"
title = "Filter Papers"
draft = false
image = "img/filterpads/round_cropped_filter_paper.png"
showonlyimage = false
weight = 4
+++

Filter Papers are typically used in fast or coarse filtration processes.  The crucial technical parameters of filter papers are porosity, busting factor, particle retention character,  and flow rate. 
<!--more-->

Filter paper is manufactured using different types of virgin wood pulps. The pulp could be of type either softwood or hardwood. The premium grade filter papers are treated with impregnation to get the right blend of qualities to suit the target application's filtration requirements.

{{< figure src="/img/filterpads/filter_paper_spec.jpg" class="img-shadow" >}}
