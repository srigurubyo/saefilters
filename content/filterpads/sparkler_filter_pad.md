+++
showonlyimage = false
draft = false
image = "img/filterpads/sparkler_pads_2_cropped.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Sparkler Filter Pads"
weight = 1
+++
---


Sparkler Filter pads are concentric circles that are placed together in the assembly of a filter cartridge. The assembly's inside shell contains prearranged filter plates with perforated supporting panels, sparkler filter sheets, and inter-locks. The assembly is complete with pump and piping attachment mounted on a trolley made from stainless steel.

The contaminated liquid to filter is fed by a pump into the filter shell. It flows through the top of each plate, through the sides of the plates opening. As the liquid pressure rises, the filter media (Paper / Fabric) keeps the impurities back and allows the crystal clear filtrate to pass through the central channel (formed by interlocking cups) that brings the filtrate to the filter exit.

Usually, sparkler filter pads are used to make the fluid clear. Often it also gets the cake that has formed between the plate. Filtration is continuous until its rate decreases due to an increased resistance to cake.

#### Our range of sparkler filter pads

* We provide both disposable and reusable Sparkler filter pads made from woven and nonwoven fabrics.
* The fabrics include polypropylene, nylon, cotton, and polyester.
* Specialty fabrics are used in some situations, too.
*  These are used extensively on sparkler filter presses in the pharmaceutical and chemical industries.
*  Many of our sparkler filter pads are heat sealed or have edges reinforced to give longer life.
* Broad choice of media ensures a high chemical compatibility and good resistance to the slurry form.

