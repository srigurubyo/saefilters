+++
image = "img/filterpads/activated_carbon_pad.png"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Carbon Pads"
draft = false
weight = 2
+++

Carbon has been used for many years in loose form to remove color and odor from liquids.
<!--more-->

In certain applications the loose carbon can be replaced with a carbon impregnated filter pad. When using loose carbon, it is necessary to remove the carbon with some means of filtration. If that includes a plate and frame filter press, then the press can be dressed with activated carbon filter pads, eliminating the need for dumping loose carbon into a mixing tank, and then scraping the carbon off of a filter paper or cloth.

We offer its series in different flavour and are manufactured with various different grades of carbon, allowing to help optimize your filtration.

{{< figure src="/img/filterpads/carbon_pad_spec.jpg" class="img-shadow" >}}
	