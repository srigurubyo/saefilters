+++
showonlyimage = false
draft = false
image = "img/filterpads/square_filter_pad.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Pulp Filter Pads"
weight = 3
+++

We manufacture a wide range of filter papers & pads on a very highly sophisticated production plant. The entire process of manufacturing is untouched by hand from the pulping stage to the finished product. 
<!--more-->

This is a high precision completely automatic plant which ensures a uniformly produced filter media for excellent purity and frees from external contaminants qualities vital for sensitive industries like drugs, pharmaceuticals, foods, beverages, chemicals, cosmetics etc.

#### Filter pads available for rough & normal filtration 
{{< figure src="/img/filterpads/mmcr_spec.jpg" class="img-shadow" >}}
---
</br>


#### Filter pads for clarifying and polishing type of filter processs
{{< figure src="/img/filterpads/mmc_spec.jpg" class="img-shadow" >}}
---
</br>


#### Filter pads for fine and premium filtration
{{< figure src="/img/filterpads/mmcw_spec.jpg" class="img-shadow" >}}
---

Our raw material is continuously monitored for quality control. Constant laboratory tests are conducted to check filtration efficiency, pad porosity, absorbency, Filtration speed and bursting factor. The fibers of our filter paper & pads are also subjected to a special porous chemically inert wet-strength impregnation process to impart very high mechanical strength while filtering aqueous.

* Apart from standard specifications, we use On-Demand approach to suite your custom requirements
* The raw material used in the manufacturing of the filter pads is virgin pulp and is unbleached
* The filter pads are manufactured and continuously monitored under the rules of food act so it is food gradable
