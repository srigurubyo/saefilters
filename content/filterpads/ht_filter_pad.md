+++
image = "img/filterpads/food_pot_bw.jpg"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "High Temperature Filter Pads"
draft = false
weight = 5
+++

Heat resistant high temperature filter pads are technically advanced filtration medium used to filter edible cooking oils or other liquids/chemicals where the operating temperature is within range of 180° C. to 250° C.
<!--more-->

They facilitate filtration of particulate materials from cooking oil used in a frying process.  They are typically used in depth filtration scenarios.

They are food grade safe and provide better tasting of filtered items and removes fatty substances that otherwise lasts longer.  They are available in various dimensions and are widely used in hotels, restaurants & across food industry.

{{< figure src="/img/filterpads/ht_filter_pad_spec.jpg" class="img-shadow" >}}
	
