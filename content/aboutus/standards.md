+++
showonlyimage = false
draft = false
image = "img/aboutus/check_list.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Standards & Certifications"
weight = 4
+++
<!--more-->
{{< figure src="/img/aboutus/check_list_cropped.jpg" class="img-shadow-small">}}
---


* ISO 9001-2000 Certified manufacturing unit
* Dun & Bradstreet Certified manufacturing unit
* 100% secure and protected from any external adulteration/contamination
* We do not use any solvent / chemical and the adhere to the strict norms of Food & Pharmaceutical Industries
* Fully automated and lesser human intervention for the high quality and safety of pads
