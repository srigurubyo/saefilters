+++
showonlyimage = false
draft = false
image = "img/aboutus/single_leaf.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Company"
weight = 1
+++

<!--more-->

**SB Tech's** filtration is a division of Sri Bharathi Group of Companies.  We are India's leading manufacturer and supplier of sparkler pulp filter pads, filter papers, modular/lenticular filters, all types of filter cartridges (PP - Polypropylene Pleated, PTFE, Stainless Steel, Spun Bonded & Wound) and other filter media for chemical, pharmaceutical, beverages, distillery and food industries. We manufacture a wide spectrum of Filter Pads that is available in varied micron rates and sizes. These filter pads are widely used in pharmaceutical industries and chemical industries. We customize our range of filter pads as per the specification detailed by the clients.

**Sri Bharathi Group of companies** is established by an eminent visionary with more than 40 years of industrial deep-rooted experience and expertise keeping the outlook on the fast-flourishing technical advances of the filtration industry.

{{< figure src="/img/aboutus/globe.png" class="img-shadow-small">}}
---

Sri Bharathi Group is committed to the sublime aspect of maintaining precise standards of it’s products.  We have scrupulously developed high quality filter pads, filter bags and industrial filtering equipment. Our continued endeavour in the avenue of research and development has brought good dividend.  Our lofty ethical principles are commensurate with our promises.

Our institutions’ vision is to execute orders qualitatively and quantitatively with prompt deliveries.  We are glad to inform you that our company has been processing orders successfully for the premier engineering institutions in India.  We assure our best and prompt service and are committed ourselves to the supreme satisfaction of our customers.
