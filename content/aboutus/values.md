+++
showonlyimage = false
draft = false
image = "img/aboutus/butterfly.png"
date = "2016-11-05T18:25:22+05:30"
title = "Values"
weight = 3
+++
<!--more-->
{{< figure src="/img/aboutus/butterfly_cropped.png" class="img-shadow-small" >}}
---

* Joint venture with National Filter Corporation and we provide best quality & professional after sales services
* Highly customizable specification in terms of crucial aspects like Fiber Length, Viscosity, Moisture etc
* Use On-Demand approach to tailor the product based on your need to suite your requirements
* Supply Filter Pads which are manufactured using: Wood Pulp, Cotton Pulp and Hybrid Pulp
* Adheres to high quality standards
* Competitive pricing
* Protected from the dollar exchange rate fluctuation
* Offers 24*7 support
* Certified for usage in Food & Pharmaceutical industries
* Offers end-to-end support which includes Water Proof Packaging, Transportation, etc

