+++
showonlyimage = false
draft = false
image = "img/aboutus/paper_boat_small.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Strength"
weight = 2
+++
<!--more-->
{{< figure src="/img/aboutus/paper_boat_small_cropped.jpg" class="img-shadow-small">}}
---

* We have tie-up with technical leaders and other organizations in the Filtering Industry 
* Manufacturing of High Quality Filter Pads, Filter Bags and other Industrial Filtering Equipment
* Our continuous Research & Developments are tailored to the most recent technological up heaves
* Its activities are governed by the ethical principles
* Has state of the art technology for the Filter Pads manufacturing
* Eminent subject matter experts in the filed of Filter Pads & Filtering Technology
* Recognized as a reputed & quality supplier by multiple leading companies in India
